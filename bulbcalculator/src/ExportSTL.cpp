/*
Copyright (C) 2011-2015  Gianluca Montecchi <gian@grys.it>

This file is part of BulbCalculator.

BulbCalculator is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 2 of the License, or (at your
option) any later version.

BulbCalculator is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with BulbCalculator.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDebug>

#include "../include/BulbCalculator.h"
#include "../include/ExportSTL.h"

#include "ui_ExportSTL.h"


ExportStl::ExportStl(QWidget *parent) : QDialog(parent), ui(new Ui::ExportStl) {

    ui->setupUi(this);
    QStringList res;
    res << tr("Low") << tr("Medium") << tr("High") << tr("Highest");
    ui->CB_STLResolution->addItems(res);
    ui->CB_STLResolution->setCurrentIndex(3);
    connect( ui->RB_XYPlane, SIGNAL( toggled(bool) ), this, SLOT( EnableTopBottom()) );
    connect( ui->RB_XZPlane, SIGNAL(toggled(bool) ), this, SLOT( EnableLeftRight()) );
    connect( ui->SP_PartNumber, SIGNAL(valueChanged(int)) , this, SLOT(UpdatePartsLength(int)));
    connect( ui->LE_BaseStlFileName, &QLineEdit::textChanged, this, &ExportStl::ValidateInput);
    connect( ui->TW_ExportOptions, &QTabWidget::currentChanged, this, &ExportStl::CurrentTabChanged);
    connect( ui->TB_SelectDirectory, &QToolButton::clicked, this, &ExportStl::SelectExportDir);
    ui->B_DialogButtons->button(QDialogButtonBox::Ok)->setEnabled(false);
    ui->RB_XYPlane->setChecked(true);
    ui->RB_TopSim->setChecked(true);
    this->setExportType(PRINT3D);

}

void ExportStl::CurrentTabChanged(int index) {

    if (index == 0) {
        if (!ui->LE_BaseStlFileName->text().isEmpty()) {
            this->setExportType(PRINT3D);
            ui->B_DialogButtons->button(QDialogButtonBox::Ok)->setEnabled(true);
        } else {
            ui->B_DialogButtons->button(QDialogButtonBox::Ok)->setEnabled(false);
        }
    } else {
        this->setExportType(CNC);
        ui->B_DialogButtons->button(QDialogButtonBox::Ok)->setEnabled(true);

    }

}

void ExportStl::SelectExportDir() {


    QString msg = tr("Select target directory");
    QFileDialog dlg(this);
    dlg.setWindowTitle(msg);
    dlg.setFileMode(QFileDialog::DirectoryOnly);
    dlg.setAcceptMode(QFileDialog::AcceptSave);
    if (dlg.exec() == QDialog::Rejected) {
        return;
    }
    this->targetDirectory = dlg.selectedFiles()[0];
    ui->LE_ExportDir->setText(this->targetDirectory );

}

QString ExportStl::getTargetDirectory() const
{
    return targetDirectory;
}

void ExportStl::setTargetDirectory(const QString &value)
{
    targetDirectory = value;
}

QString ExportStl::getBaseFileName() const
{
    return baseFileName;
}

int ExportStl::getExportType() const
{
    return exportType;
}

void ExportStl::setExportType(int value)
{
    exportType = value;
}

void ExportStl::ValidateInput(QString text) {

    if (!text.isEmpty()) {
        ui->B_DialogButtons->button(QDialogButtonBox::Ok)->setEnabled(true);
        this->baseFileName = QString(text);
    } else {
        ui->B_DialogButtons->button(QDialogButtonBox::Ok)->setEnabled(false);
    }

}


void ExportStl::EnableTopBottom() {

    ui->RB_LeftSim->setEnabled(false);
    ui->RB_RightSim->setEnabled(false);
    ui->RB_TopSim->setEnabled(true);
    ui->RB_BottomSim->setEnabled(true);
    ui->RB_TopSim->setChecked(true);

}

void ExportStl::EnableLeftRight() {

    ui->RB_LeftSim->setEnabled(true);
    ui->RB_RightSim->setEnabled(true);
    ui->RB_TopSim->setEnabled(false);
    ui->RB_BottomSim->setEnabled(false);
    ui->RB_RightSim->setChecked(true);

}

void ExportStl::UpdatePartsLength(int numberofparts) {

    ui->LB_PartsLength->setText(QString::number(bulblen/numberofparts, '.', 2));

}

ExportStl::~ExportStl() {

    delete ui;

}


int ExportStl::getResolution(void) {


    switch(ui->CB_STLResolution->currentIndex()) {
        case 0:
            return RES_LOW;
        case 1:
            return RES_MED;
        case 2:
            return RES_HIGH;
        case 3:
            return RES_HIGHEST-1;
    }
    return -1;

}

void ExportStl::SetBulbLenght(double length) {

    bulblen = length;
    ui->L_BulbLength->setText(QString::number(bulblen,'.',2));

}

void ExportStl::SetMeasureUnit(QString unit) {

    ui->L_Units1->setText(unit);
    ui->L_Units2->setText(unit);
}

int ExportStl::GetSimmetry(void) {

    if(ui->RB_TopSim->isChecked() == true) {
        return OBJECT_HALF_TOP;
    } else if (ui->RB_BottomSim->isChecked() == true) {
        return OBJECT_HALF_BOTTOM;
    } else if(ui->RB_LeftSim->isChecked() == true) {
        return OBJECT_HALF_LEFT;
    } else if(ui->RB_RightSim->isChecked() == true) {
        return OBJECH_HALF_RIGHT;
    }
    return -1;

}

int ExportStl::getHalf(void) {

    int obj;

    obj = OBJECT_FULL;
    if (ui->GB_HalfObject->isChecked() == true) {
        if (ui->RB_XYPlane->isChecked() == true) {
            obj = OBJECT_HALF_XY;
        }
        if (ui->RB_XZPlane->isChecked() == true) {
            obj = OBJECT_HALF_XZ;
        }
    }

    return obj;
}

int ExportStl::getPartsNumber(void) {

    return ui->SP_PartNumber->value();

}
