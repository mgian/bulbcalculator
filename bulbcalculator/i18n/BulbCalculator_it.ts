<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="en">
<context>
    <name>BcPreference</name>
    <message>
        <location filename="../src/Prefereces.cpp" line="73"/>
        <source>Some preferences where modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Prefereces.cpp" line="74"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Prefereces.cpp" line="99"/>
        <source>Select loacal repository</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BulbCalculator</name>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="123"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="123"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="123"/>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="123"/>
        <source>Highest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="128"/>
        <source>Wireframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="128"/>
        <source>Triangles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="128"/>
        <source>Surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="132"/>
        <source>3D resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="133"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="134"/>
        <source>View Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="309"/>
        <source>Export Text Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="311"/>
        <source>Text Files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="316"/>
        <location filename="../src/BulbCalculator.cpp" line="362"/>
        <location filename="../src/BulbCalculator.cpp" line="371"/>
        <location filename="../src/BulbCalculator.cpp" line="1018"/>
        <location filename="../src/BulbCalculator.cpp" line="1104"/>
        <location filename="../src/BulbCalculator.cpp" line="1129"/>
        <location filename="../src/BulbCalculator.cpp" line="1621"/>
        <location filename="../src/BulbCalculator.cpp" line="1732"/>
        <source>BulbCalculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="317"/>
        <location filename="../src/BulbCalculator.cpp" line="1019"/>
        <location filename="../src/BulbCalculator.cpp" line="1130"/>
        <source>No filename given, aborting operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="363"/>
        <source>Simmetry Value incorrect
Aborting operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="372"/>
        <source>The resolution is wrong
Aborting operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="388"/>
        <source>Export Ascii STL File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="391"/>
        <source>Export Binary STL File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="440"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="443"/>
        <source>Copyright 2000, 2001 by Marko Majic (Some Rights Reserved)&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="445"/>
        <source>Contact: gian@grys.it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="447"/>
        <source>About BulbCalculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="466"/>
        <source>2d View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="484"/>
        <source>3d View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="503"/>
        <source>General data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="504"/>
        <source>Sections data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="510"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="514"/>
        <source>Lenght / Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="514"/>
        <source>Material density</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="515"/>
        <source>Center / Lenght</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="515"/>
        <source>Projected Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="516"/>
        <source>Wetted Surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="516"/>
        <source>Max Diameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="516"/>
        <source>Frontal Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="559"/>
        <source>Bulb Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="573"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="574"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="575"/>
        <source>Height Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="576"/>
        <source>Height Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="577"/>
        <source>Half Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="578"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="972"/>
        <source>The file already exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="973"/>
        <source>Do you want to overwrite it ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1105"/>
        <source>Cannot save target file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1812"/>
        <location filename="../src/BulbCalculator.cpp" line="1877"/>
        <location filename="../src/BulbCalculator.cpp" line="1901"/>
        <source>Import foil data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1813"/>
        <location filename="../src/BulbCalculator.cpp" line="1902"/>
        <source>The profile cannot be imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1853"/>
        <source>Do yuo want to import the foil to the local repository ?&lt;p&gt;&lt;b&gt;If not, the project cannot be saved&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1878"/>
        <source>Cannot copy the file to the local reporitory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="2117"/>
        <source>&amp;%1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="514"/>
        <location filename="../src/BulbCalculator.cpp" line="581"/>
        <source>Lenght</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="582"/>
        <source>Projected weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="514"/>
        <location filename="../src/BulbCalculator.cpp" line="583"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="444"/>
        <source>Copyright 2010, 2015 by Gianluca Montecchi (Some Rights Reserved)&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="516"/>
        <location filename="../src/BulbCalculator.cpp" line="584"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="585"/>
        <source>Wetted surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="941"/>
        <location filename="../src/BulbCalculator.cpp" line="1576"/>
        <source>The design has been modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="942"/>
        <location filename="../src/BulbCalculator.cpp" line="1577"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1002"/>
        <source>The project cannot be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1003"/>
        <source>The foil definition was not imported in the local repository.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1596"/>
        <source>Bulb (*.blb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1185"/>
        <source>Bulb Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1594"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1622"/>
        <source>Cannot open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbCalculator.cpp" line="1733"/>
        <source>Not a valid file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BulbDataOptions</name>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="109"/>
        <source>Lenght (cm):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="110"/>
        <source>Projected Weight (Kg):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="111"/>
        <source>Volume (cm³):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="112"/>
        <source>Wetted Surface (cm²):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="113"/>
        <source>Slice thickness (cm):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="131"/>
        <location filename="../src/BulbDataOptions.cpp" line="156"/>
        <source>Lenght (in):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="132"/>
        <location filename="../src/BulbDataOptions.cpp" line="157"/>
        <source>Projected Weight (oz):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="133"/>
        <location filename="../src/BulbDataOptions.cpp" line="158"/>
        <source>Volume (cu.in.):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="134"/>
        <location filename="../src/BulbDataOptions.cpp" line="159"/>
        <source>Wetted Surface (sq.in.):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BulbDataOptions.cpp" line="135"/>
        <location filename="../src/BulbDataOptions.cpp" line="160"/>
        <source>Slice thickness (in.):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DBulbDataOptions</name>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="14"/>
        <location filename="../Ui/BulbDataOptions.ui" line="28"/>
        <source>Bulb Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="34"/>
        <source>Lenght (cm):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="72"/>
        <source>Projected Weight (kg):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="107"/>
        <source>Volume (cm³):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="142"/>
        <source>Wetted Surface (cm²):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="180"/>
        <source>Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="186"/>
        <source>&amp;Metric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="189"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="199"/>
        <source>&amp;Imperial (fractions)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="202"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="209"/>
        <source>Impe&amp;rial (decimals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="223"/>
        <source>Section Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="231"/>
        <source>Numebr of sections:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="264"/>
        <source>Even</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="274"/>
        <source>Cosine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="286"/>
        <source>Slice distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="292"/>
        <source>Slice thickness (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="321"/>
        <source>Reference points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="327"/>
        <source>Number of reference point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbDataOptions.ui" line="347"/>
        <source>Show reference points</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DBulbPrintOptions</name>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="14"/>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="24"/>
        <source>Print Positioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="30"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="37"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="44"/>
        <source>Use Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="51"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="61"/>
        <source>Offsets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="67"/>
        <source>Printing X Offset (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="87"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="110"/>
        <source>Print view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="116"/>
        <source>Plan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="123"/>
        <source>Side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/BulbPrintOptions.ui" line="130"/>
        <source>Sections</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>D_BulbParam</name>
    <message>
        <location filename="../Ui/SetBulbParameterDialog.ui" line="17"/>
        <source>Set Bulb Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/SetBulbParameterDialog.ui" line="29"/>
        <source>Target Weight (Kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/SetBulbParameterDialog.ui" line="42"/>
        <source>Material Density (Kg/dm3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/SetBulbParameterDialog.ui" line="58"/>
        <source>Height/Lenght Ratio (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/SetBulbParameterDialog.ui" line="71"/>
        <source>Width/Height Ratio (%)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>D_Preferences</name>
    <message>
        <location filename="../Ui/Preferences.ui" line="17"/>
        <source>BulbCalculator - Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="80"/>
        <location filename="../Ui/Preferences.ui" line="133"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="92"/>
        <location filename="../Ui/Preferences.ui" line="377"/>
        <source>Bulb defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="101"/>
        <source>Data repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="209"/>
        <source>User interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="224"/>
        <source>Tabbed Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="237"/>
        <source>Windows mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="256"/>
        <source>Tab position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="272"/>
        <source>MDI Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="292"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="297"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="302"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="307"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="151"/>
        <source>Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="157"/>
        <source>&amp;Metric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="160"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="170"/>
        <source>Impe&amp;rial (decimals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="177"/>
        <source>&amp;Imperial (fractions)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="180"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="324"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="336"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="349"/>
        <source>White</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="389"/>
        <source>Default parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="407"/>
        <source>Height/Lenght Ratio (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="485"/>
        <source>Target Weight (Kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="507"/>
        <source>Material Density (Kg/dm3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="551"/>
        <source>Width/Height Ratio (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="595"/>
        <source>Section Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="603"/>
        <source>Numebr of sections:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="633"/>
        <source>Even</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="643"/>
        <source>Cosine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="661"/>
        <source>Miscellanous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="689"/>
        <source>Slice thickness (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="708"/>
        <source>Show reference points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="728"/>
        <source>Draw slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="743"/>
        <source>Data Repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="755"/>
        <source>Remote repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="783"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="803"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="823"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="858"/>
        <source>Local repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/Preferences.ui" line="884"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportFile3D</name>
    <message>
        <location filename="../src/ExportFile3D.cpp" line="130"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFile3D.cpp" line="130"/>
        <source>Could not open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFile3D.cpp" line="134"/>
        <source>Exporting to ASCII STL file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFile3D.cpp" line="178"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportFileData</name>
    <message>
        <location filename="../src/ExportFileData.cpp" line="80"/>
        <location filename="../src/ExportFileData.cpp" line="126"/>
        <location filename="../src/ExportFileData.cpp" line="166"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="80"/>
        <location filename="../src/ExportFileData.cpp" line="126"/>
        <location filename="../src/ExportFileData.cpp" line="166"/>
        <source>Could not open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="87"/>
        <source>Foil Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="88"/>
        <source>Height/Length Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="89"/>
        <source>Width/Height Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="91"/>
        <source>Lenght (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="92"/>
        <source>Center (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="93"/>
        <source>Wetted Surface (cm^2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="94"/>
        <source>Volume (cm^3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="96"/>
        <location filename="../src/ExportFileData.cpp" line="138"/>
        <location filename="../src/ExportFileData.cpp" line="178"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="97"/>
        <location filename="../src/ExportFileData.cpp" line="179"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="98"/>
        <location filename="../src/ExportFileData.cpp" line="180"/>
        <source>Height Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="99"/>
        <location filename="../src/ExportFileData.cpp" line="181"/>
        <source>Height Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="100"/>
        <location filename="../src/ExportFileData.cpp" line="182"/>
        <source>Width/2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="103"/>
        <location filename="../src/ExportFileData.cpp" line="104"/>
        <location filename="../src/ExportFileData.cpp" line="105"/>
        <location filename="../src/ExportFileData.cpp" line="106"/>
        <source>(cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="133"/>
        <location filename="../src/ExportFileData.cpp" line="173"/>
        <source>Lenght (in.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="134"/>
        <location filename="../src/ExportFileData.cpp" line="174"/>
        <source>Center (in.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="135"/>
        <location filename="../src/ExportFileData.cpp" line="175"/>
        <source>Wetted Surface (sq.in.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="136"/>
        <location filename="../src/ExportFileData.cpp" line="176"/>
        <source>Volume (cu.in.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="139"/>
        <source>X (in.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="140"/>
        <source>Height Max (in.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="141"/>
        <source>Height Min (in.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="142"/>
        <source>Width/2 (in.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportFileData.cpp" line="145"/>
        <location filename="../src/ExportFileData.cpp" line="146"/>
        <location filename="../src/ExportFileData.cpp" line="147"/>
        <location filename="../src/ExportFileData.cpp" line="148"/>
        <location filename="../src/ExportFileData.cpp" line="185"/>
        <location filename="../src/ExportFileData.cpp" line="186"/>
        <location filename="../src/ExportFileData.cpp" line="187"/>
        <location filename="../src/ExportFileData.cpp" line="188"/>
        <source>(in.)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportStl</name>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="14"/>
        <source>STL Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="22"/>
        <source>STL File Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="28"/>
        <source>Ascii</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="41"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="59"/>
        <source>Tab 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="71"/>
        <source>Parts to split</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="91"/>
        <source>Bulb Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="104"/>
        <source>Parts length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="139"/>
        <source> Base file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="150"/>
        <source>Tab 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="156"/>
        <source>Half Ob&amp;ject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="168"/>
        <source>Simmetry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="174"/>
        <source>&amp;XY Plane (Horizontal simmetry)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="181"/>
        <source>X&amp;Z Plane (Vertical simmetry)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="191"/>
        <source>Side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="197"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="204"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="211"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/ExportSTL.ui" line="218"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportSTL.cpp" line="32"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportSTL.cpp" line="32"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportSTL.cpp" line="32"/>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportSTL.cpp" line="32"/>
        <source>Highest</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Ui/MainWindow.ui" line="14"/>
        <source>Bulb Calculator - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="51"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="101"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="110"/>
        <source>&amp;Bulb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="55"/>
        <source>&amp;Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="62"/>
        <source>P&amp;rint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="70"/>
        <source>&amp;Sections...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="77"/>
        <source>&amp;Linesplan...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="114"/>
        <source>D&amp;ownload Airfoil Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="128"/>
        <source>&amp;3D Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="135"/>
        <source>Wi&amp;ndow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="149"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="170"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="185"/>
        <source>&amp;New Bulb design</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="188"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="200"/>
        <source>&amp;Open Bulb design</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="203"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="215"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="218"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="230"/>
        <source>Save &amp;as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="233"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="242"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="245"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="250"/>
        <location filename="../Ui/MainWindow.ui" line="378"/>
        <source>&amp;Cascade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="253"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="258"/>
        <location filename="../Ui/MainWindow.ui" line="373"/>
        <source>&amp;Tile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="261"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="266"/>
        <source>&amp;3D view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="271"/>
        <location filename="../Ui/MainWindow.ui" line="464"/>
        <source>Calculation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="276"/>
        <source>S&amp;ide view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="281"/>
        <source>&amp;Top view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="286"/>
        <source>&amp;Bulb dimension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="291"/>
        <source>&amp;Calculation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="296"/>
        <source>&amp;Set Bulb Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="301"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="518"/>
        <source>&amp;STL File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="341"/>
        <source>Show Side &amp;view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="350"/>
        <source>Show &amp;3D view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="368"/>
        <source>Show &amp;Bulb Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="383"/>
        <source>Bulb &amp;Data Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="459"/>
        <source>&amp;Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="473"/>
        <location filename="../Ui/MainWindow.ui" line="487"/>
        <source>From &amp;Side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="508"/>
        <source>&amp;Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="513"/>
        <source>&amp;Page setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="528"/>
        <source>&amp;Keel Slot Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="523"/>
        <source>&amp;Binary STL File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="312"/>
        <source>Show &amp;Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="323"/>
        <source>Show &amp;Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="332"/>
        <source>Show &amp;2D view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="359"/>
        <source>Show Calculation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="388"/>
        <source>Print options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="397"/>
        <location filename="../Ui/MainWindow.ui" line="416"/>
        <source>&amp;From Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="402"/>
        <source>&amp;Text File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="407"/>
        <source>&amp;Import Foil Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="419"/>
        <location filename="../Ui/MainWindow.ui" line="422"/>
        <source>From Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="430"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="438"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="446"/>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="454"/>
        <source>Highest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="478"/>
        <source>Foils data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/MainWindow.ui" line="496"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintDraw</name>
    <message>
        <location filename="../src/PrintBulb.cpp" line="137"/>
        <source>Bulb data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="139"/>
        <source>Lenght</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="140"/>
        <source>Material Density</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="141"/>
        <source>Projected weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="142"/>
        <location filename="../src/PrintBulb.cpp" line="166"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="143"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="144"/>
        <source>Wetted surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="148"/>
        <location filename="../src/PrintBulb.cpp" line="151"/>
        <location filename="../src/PrintBulb.cpp" line="189"/>
        <location filename="../src/PrintBulb.cpp" line="191"/>
        <source> cm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="150"/>
        <source> Kg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="149"/>
        <source> gr/cm�</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="152"/>
        <location filename="../src/PrintBulb.cpp" line="153"/>
        <location filename="../src/PrintBulb.cpp" line="192"/>
        <source> cm�</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="155"/>
        <location filename="../src/PrintBulb.cpp" line="156"/>
        <location filename="../src/PrintBulb.cpp" line="158"/>
        <location filename="../src/PrintBulb.cpp" line="196"/>
        <location filename="../src/PrintBulb.cpp" line="198"/>
        <source> in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="157"/>
        <source> oz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="159"/>
        <source> cu.in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="160"/>
        <location filename="../src/PrintBulb.cpp" line="199"/>
        <source> sq.in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="164"/>
        <source>Lenght/Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="165"/>
        <source>Width/Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="167"/>
        <source>Center/Lenght</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="168"/>
        <source>Max Diam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="169"/>
        <source>Frontal area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="203"/>
        <source>Sections data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="207"/>
        <location filename="../src/PrintBulb.cpp" line="224"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="208"/>
        <location filename="../src/PrintBulb.cpp" line="225"/>
        <source>X Pos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="209"/>
        <location filename="../src/PrintBulb.cpp" line="226"/>
        <source>Height Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="210"/>
        <location filename="../src/PrintBulb.cpp" line="227"/>
        <source>Height Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="211"/>
        <location filename="../src/PrintBulb.cpp" line="228"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="255"/>
        <source>BulbCalculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PrintBulb.cpp" line="256"/>
        <source>The draw is bigger than the printing area</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QKeelOptions</name>
    <message>
        <location filename="../Ui/KeelOptions.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/KeelOptions.ui" line="20"/>
        <source>Create &amp;Keel Slot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/KeelOptions.ui" line="45"/>
        <location filename="../Ui/KeelOptions.ui" line="113"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/KeelOptions.ui" line="65"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/KeelOptions.ui" line="85"/>
        <source>lenght</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/KeelOptions.ui" line="95"/>
        <source>Screw Hole</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui/KeelOptions.ui" line="133"/>
        <source>Diameter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewArea</name>
    <message>
        <location filename="../src/ViewArea.cpp" line="111"/>
        <location filename="../src/ViewArea.cpp" line="117"/>
        <location filename="../src/ViewArea.cpp" line="123"/>
        <source>Profile: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ViewArea.cpp" line="113"/>
        <source> - Side view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ViewArea.cpp" line="119"/>
        <source> - Top view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ViewArea.cpp" line="125"/>
        <source> - Front view</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
